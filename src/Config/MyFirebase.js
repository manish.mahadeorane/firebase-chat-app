import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyAmWanZ5zpaE1-YSQ4rHIeuMXwXXN7gEho",
  authDomain: "fir-react-chat-task.firebaseapp.com",
  databaseURL: "https://fir-react-chat-task.firebaseio.com",
  projectId: "fir-react-chat-task",
  storageBucket: "fir-react-chat-task.appspot.com",
  messagingSenderId: "72527886009",
  appId: "1:72527886009:web:773790d5e2f707c0"
}
firebase.initializeApp(config)
firebase.firestore().settings({
  timestampsInSnapshots: true
})

export const myFirebase = firebase
export const myFirestore = firebase.firestore()
export const myStorage = firebase.storage()
